<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SARIS | <?php echo (isset($title) ? $title : ''); ?></title>

    <!--Datatable cdns-->
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/scroller/1.4.2/css/scroller.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/keytable/2.2.1/css/keyTable.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/rowgroup/1.0.0/css/rowGroup.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/fixedcolumns/3.2.2/css/fixedColumns.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/colreorder/1.3.3/css/colReorder.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/autofill/2.2.0/css/autoFill.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet">
    <!-- Theme style -->

    <link href="<?php echo base_url(); ?>media/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>media/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>media/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>media/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>media/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>media/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>media/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>media/css/select2-bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>media/css/jquery-confirm.min.css" rel="stylesheet">


    <link href="<?php echo base_url(); ?>media/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>media/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url(); ?>media/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>media/js/jquery-confirm.min.js"></script>
    <script src="<?php echo base_url(); ?>media/js/script.js"></script>
    <script src="<?php echo base_url(); ?>media/js/html-table-search.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url(); ?>media/js/plugins/select2/select2.full.min.js"></script>

<!-- Select2 -->
    <script src="<?php echo base_url(); ?>media/js/easy-ticker.js"></script>
    <script src="<?php echo base_url(); ?>media/js/jquery.easing.min.js"></script>

    <?php
    $organisation_info = get_collage_info();
    $CURRENT_USER = current_user();
    ?>
</head>

<body>

<div id="wrapper">
    <div id="header_div" style="background-color: #49688e;  border-bottom: 5px solid #f4a024;">
        <?php
        include 'include/header.php';
        ?>


    </div>
    <div id="header_line"></div>
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php
            include 'include/leftmenu.php';
            ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <?php
        include 'include/bscrum.php';
        ?>
        <div style="border-bottom: 1px solid #2f4050; text-align: right; padding: 2px; color: #2f4050; font-weight: bold;">
            <?php
            $active_y = $this->common_model->get_academic_year(null, 1, 1)->row();
            echo 'Active Academic Year : '.$active_y->AYear.' - '.$active_y->semester;
            ?>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight" >
            <?php
            if (isset($content) && isset($data)) {
                $this->load->view($content, $data);
            } else {
                $this->load->view($content);
            }
            ?>
        </div>
        <div class="footer fixed" style="font-size: 11px;">
            <div class="pull-right">
                Design and Developed by <strong><a href="http://www.zalongwa.com" target="_blank">ZALONGWA TECHNOLOGIES</a></strong>.
            </div>
            <div>
                <strong> &copy; 2012- <?php echo date('Y'); ?> &nbsp; &nbsp; <a
                        href="<?php echo $organisation_info->Site; ?>"
                        target="_blank"><?php echo $organisation_info->Name; ?></a></strong>
            </div>
        </div>

    </div>
</div>


<script src="<?php echo base_url(); ?>media/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>media/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>media/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>media/js/plugins/pace/pace.min.js"></script>

<!-- Data picker -->
<script src="<?php echo base_url(); ?>media/js/plugins/datapicker/bootstrap-datepicker.js"></script>



<script>
    $(document).ready(function(){

            $(this).bind("contextmenu", function(e) {
               // e.preventDefault();
            });

        $('.mydate_input').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy"
        });

        $('#datatable').dataTable({

            dom: "Bfrtip",
            "order": [],
            buttons:
                [


                    {

                        extend: 'colvis',
                        columns: ':not(.noVis)'
                    }

                ]


        });

    })


    function numbersonly(e,value){
        var unicode=e.charCode? e.charCode : e.keyCode
        if (unicode!=8 && unicode!=46 && unicode!=9){ //if the key isn't the backspace key (which we should allow)
            if (unicode<48||unicode>57) //if not a number
                return false //disable key press
        }
    }
</script>

<!-- ******************************POP UP************************************ -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModal" aria-hidden="true"
     data-backdrop="dynamic" data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="display: none;" id="model_header">
                <h4 class="modal-title"></h4>
                </div>
            <div class="modal-body" id="modal_body" style="z-index: 999999 !important;">
            <div style="height: 100px; line-height: 100px; text-align: center;"><img
                    src="<?php echo base_url(); ?>icon/loader.gif"/> Loading....
            </div>
                </div>
            <div class="modal-footer" style="display: none;" id="model_footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/keytable/2.2.1/js/dataTables.keyTable.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/scroller/1.4.2/js/dataTables.scroller.min.js"></script>
<script src="https://cdn.datatables.net/autofill/2.2.0/js/dataTables.autoFill.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
<script src="https://cdn.datatables.net/colreorder/1.3.3/js/dataTables.colReorder.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
<script src="https://cdn.datatables.net/rowgroup/1.0.0/js/dataTables.rowGroup.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"></script>


</body>

</html>


