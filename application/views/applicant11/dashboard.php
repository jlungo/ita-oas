<div class="ibox">
    <div class="ibox-heading">
        <div class="ibox-title">Dashboard</div>
    </div>
    <div class="ibox-content">Welcome !, Please use menu at the right side to navigate in the system

        <br/><br/>

        <ul>
            <li>Do you have multiple selection?: <a href="<?php echo site_url('confirmationcode'); ?>" style="font-weight: bold; text-decoration: underline;">Enter confirmation code here</a>.  </li>
            <li>Reject Admission?: <a href="<?php echo site_url('rejectAdmission'); ?>" style="font-weight: bold; text-decoration: underline;">Click here to reject your admission</a>.  </li>

        </ul>

    </div>
</div>