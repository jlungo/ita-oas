<div class="ibox">
    <div class="ibox-heading">
        <div class="ibox-title">Dashboard</div>
    </div>
    <div class="ibox-content">Welcome !, Please use menu at the right side to navigate in the system

        <br/><br/>

        <ul>
          <?php
          $userid = $CURRENT_USER->id;
          $applicant_id = $this->db->get_where('users', array('id'=>$userid))->row()->applicant_id;
          $get_f4index = $this->db->get_where('tcu_records', array('applicant_id'=>$applicant_id, 'type'=>'Confirm'))->row()->f4indexno;
          if($get_f4index){
            echo show_alert('Congratulation, you have already confirmed..!!!', 'info');
          }else{
          ?>
            <li>Do you have multiple selection?: <a href="<?php echo site_url('confirmationcode'); ?>" style="font-weight: bold; text-decoration: underline;">Enter confirmation code here</a>.  </li>
          <?php } ?>
            <li>Reject Admission?: <a href="<?php echo site_url('reject_admission'); ?>" style="font-weight: bold; text-decoration: underline;">Click here to reject your admission</a>.  </li>

        </ul>

    </div>
</div>
