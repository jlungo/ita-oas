<?php
$this->load->library('excel');
$max_column = 'L';
$this->excel->startExcel(12, $max_column);
$sheet = $this->excel->get_sheet_instance();

$sheet->setTitle("APPLICANT LIST");
$sheet->setCellValue('B3', 'APPLICANT LIST REPORT - ' . $ayear);
$programmeinfo = get_value('programme', array('Code' => $programme), null);


//Heading Main
$sheet->mergeCells('B4:' . $max_column . '4');
$sheet->setCellValue('B4', 'Programme : ' . $programmeinfo->Name);
$sheet->getStyle('B4')->getFont()->setSize(13);


$set_borders = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR
        ))
);


$rows = 6;
$column = 'A';
$sheet->setCellValue($column . $rows, 'S/N');
$column++;
$sheet->setCellValue($column . $rows, 'FIRST NAME');
$column++;
$sheet->setCellValue($column . $rows, 'SECOND NAME');
$column++;
$sheet->setCellValue($column . $rows, 'SURNAME');
$column++;
$sheet->setCellValue($column . $rows, 'DATE OF APPLICATION');
$column++;
$sheet->setCellValue($column . $rows, 'APPLICATION FEE');
$column++;
$sheet->setCellValue($column . $rows, 'CHARGES');
$column++;
$sheet->setCellValue($column . $rows, 'TOTAL');
$column++;
$sheet->setCellValue($column . $rows, 'DOB');
$column++;
$sheet->setCellValue($column . $rows, 'GENDER');
$column++;
$sheet->setCellValue($column . $rows, 'CAMPUS');
$column++;
$sheet->setCellValue($column . $rows, 'IMPAIRMENT');
$column++;
$sheet->setCellValue($column . $rows, 'FORM FOUR INDEX');
$column++;
$sheet->setCellValue($column . $rows, 'FORM FOUR YEAR');
$column++;
$sheet->setCellValue($column . $rows, 'FORM SIX INDEX');
$column++;
$sheet->setCellValue($column . $rows, 'FORM SIX YEAR');
$column++;
$sheet->setCellValue($column . $rows, 'NTA4 REG');
$column++;
$sheet->setCellValue($column . $rows, 'NTA4  GRAD YEAR');
$column++;
$sheet->setCellValue($column . $rows, 'NTA5 REG');
$column++;
$sheet->setCellValue($column . $rows, 'NTA5 GRAD YEAR');
$column++;
$sheet->setCellValue($column . $rows, 'EMAIL ADDRESS');
$column++;
$sheet->setCellValue($column . $rows, 'ADDRESS');
$column++;
$sheet->setCellValue($column . $rows, 'PHONE');
$column++;
$sheet->setCellValue($column . $rows, 'REGION');
$column++;
$sheet->setCellValue($column . $rows, 'DISTRICT');
$column++;
$sheet->setCellValue($column . $rows, 'NEXT OF KIN NAME');
$column++;
$sheet->setCellValue($column . $rows, 'NEXT OF KIN PHONE');
$column++;
$sheet->setCellValue($column . $rows, 'NEXT OF KIN ADDRESS');
$column++;
$sheet->setCellValue($column . $rows, 'NEXT OF KIN RELATION');
$column++;
$sheet->setCellValue($column . $rows, 'NEXT OF KIN REGION');
$column++;
$sheet->setCellValue($column . $rows, 'NATIONALITY');
$column++;
$sheet->setCellValue($column . $rows, 'Entry Type');
$column++;
$sheet->setCellValue($column . $rows, 'Choice#');
$column++;
$sheet->setCellValue($column . $rows, 'Point');
$column++;
$sheet->setCellValue($column . $rows, 'Eligible?');
$column++;
$sheet->setCellValue($column . $rows, 'Remark');
$column++;
$sheet->setCellValue($column . $rows, 'Form VI Results');
$column++;
$sheet->setCellValue($column . $rows, 'Form IV Results');
$column++;
$sheet->setCellValue($column . $rows, 'GPA');
$column++;
$sheet->setCellValue($column . $rows, 'Degree/Diploma/Certificate Information');

$rows++;
foreach ($applicant_list as $key => $value) {
    $form4index=$this->db->get_where('application_education_authority',array('applicant_id'=>$value->id, 'certificate'=>1))->row();
    $form6index=$this->db->get_where('application_education_authority',array('applicant_id'=>$value->id, 'certificate'=>2))->row();
    $next_of_kin_info=$this->db->get_where('application_nextkin_info',array('applicant_id'=>$value->id))->row();
    $appliction_fee = $this->db->where('applicant_id', $value->id)->get('application_payment')->row();
    $column = 'A';
    $sheet->setCellValue($column . $rows, ($key + 1));
    $column++;
    $sheet->setCellValue($column . $rows, ucfirst($value->FirstName));
    $column++;
    $sheet->setCellValue($column . $rows, ucfirst($value->MiddleName));
    $column++;
    $sheet->setCellValue($column . $rows, ucfirst($value->LastName));
    $column++;
    $sheet->setCellValue($column . $rows, ucfirst($value->createdon));
    $column++;
    $sheet->setCellValue($column . $rows, $appliction_fee->amount);
    $column++;
    $sheet->setCellValue($column . $rows, $appliction_fee->charges);
    $column++;
    $sheet->setCellValue($column . $rows, $appliction_fee->amount+$appliction_fee->charges);
    $column++;
    $sheet->setCellValue($column . $rows, $value->dob);
    $column++;
    $sheet->setCellValue($column . $rows, $value->Gender);
    $column++;
    $sheet->setCellValue($column . $rows, get_value('campus', $value->Campus, 'name'));
    $column++;
    $sheet->setCellValue($column . $rows, get_value('disability', $value->Disability, 'name'));
    $column++;
    $sheet->setCellValue($column . $rows, substr($form4index->index_number, 0 ,10));
    $column++;
    $sheet->setCellValue($column . $rows, substr($form4index->index_number, 11));
    $column++;
    $sheet->setCellValue($column . $rows, substr($form6index->index_number, 0 ,10));
    $column++;
    $sheet->setCellValue($column . $rows, substr($form6index->index_number, 11));
    $column++;
    $sheet->setCellValue($column . $rows, '');
    $column++;
    $sheet->setCellValue($column . $rows, '');
    $column++;
    $sheet->setCellValue($column . $rows, '');
    $column++;
    $sheet->setCellValue($column . $rows, '');
    $column++;
    $sheet->setCellValue($column . $rows, $value->Email);
    $column++;
    $sheet->setCellValue($column . $rows, $value->postal);
    $column++;
    $sheet->setCellValue($column . $rows, str_replace(' ', '', $value->Mobile1));
    $column++;
    $sheet->setCellValue($column . $rows, '');
    $column++;
    $sheet->setCellValue($column . $rows, '');
    $column++;
    $sheet->setCellValue($column . $rows, ucfirst($next_of_kin_info->name));
    $column++;
    $sheet->setCellValue($column . $rows, $next_of_kin_info->mobile1);
    $column++;
    $sheet->setCellValue($column . $rows, $next_of_kin_info->postal);
    $column++;
    $sheet->setCellValue($column . $rows, ucfirst($next_of_kin_info->relation));
    $column++;
    $sheet->setCellValue($column . $rows, '');
    $column++;
    $sheet->setCellValue($column . $rows, get_value('nationality', $value->Nationality, 'Name'));
    $column++;
    $sheet->setCellValue($column . $rows, entry_type_human($value->entry_category));
    $column++;
    $sheet->setCellValue($column . $rows, $value->choice);
    $column++;
    $sheet->setCellValue($column . $rows, $value->point);
    $column++;
    $sheet->setCellValue($column . $rows, ($value->eligible == 1 ? 'Yes' : 'No'));
    $column++;
    $sheet->setCellValue($column . $rows, trim($value->comment));
    $column++;


    $form6_json = json_decode($value->form6_subject);
    $form6_label = '';
    foreach ($form6_json as $pkp=>$pkv){
        $form6_label.= $pkp.' : '.json_encode($pkv)."\n";
    }
    $sheet->setCellValue($column . $rows, rtrim($form6_label,"\n"));
    $column++;

   $form4_json = json_decode($value->form4_subject);
    $form4_label = '';
    foreach ($form4_json as $pkp=>$pkv){
        $form4_label.= $pkp.' : '.json_encode($pkv)."\n";
    }
    $sheet->setCellValue($column . $rows, rtrim($form4_label,"\n"));
    $column++;

    $sheet->setCellValue($column . $rows, trim($value->gpa));
    $column++;

    $diploma_json = json_decode($value->diploma_info);
    $diploma_label = '';
    foreach ($diploma_json as $pkp=>$pkv){
        $diploma_label.= $pkp.' : '.json_encode($pkv)."\n";
    }
    $sheet->setCellValue($column . $rows, rtrim($diploma_label,"\n"));

    $rows++;
}
$max_column = $column;
$sheet->getStyle('J')->getAlignment()->setWrapText(true);
$sheet->getStyle('A6:' . $max_column . ($rows - 1))->applyFromArray($set_borders);

$this->excel->Output("APPLICANT LIST");
exit;
?>
